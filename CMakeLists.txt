cmake_minimum_required(VERSION 2.8.3)
project(autoware_map_loader)

find_package(catkin REQUIRED COMPONENTS
  autoware_build_flags
  roscpp
  tf
  geometry_msgs
  visualization_msgs
  autoware_map_msgs
  vector_map
  vector_map_msgs
)

set(CMAKE_CXX_FLAGS " -o0 -g -Wall ${CMAKE_CXX_FLAGS}")

catkin_package(
 INCLUDE_DIRS include
 LIBRARIES autoware_map
 CATKIN_DEPENDS roscpp tf visualization_msgs geometry_msgs autoware_map_msgs vector_map vector_map_msgs 
)

include_directories(
	include
  ${catkin_INCLUDE_DIRS}
)


add_library( autoware_map lib/autoware_map/autoware_map.cpp 
							lib/autoware_map/util.cpp lib/autoware_map/visualization.cpp)
add_dependencies( autoware_map ${catkin_EXPORTED_TARGETS} )
target_link_libraries( autoware_map ${catkin_LIBRARIES} )

add_executable( autoware2vectormap_converter
                nodes/autoware2vectormap_converter/autoware2vectormap_converter.cpp
                nodes/autoware2vectormap_converter/converter_core.cpp
                nodes/autoware2vectormap_converter/vmap_visualization.cpp
                nodes/autoware2vectormap_converter/converter_util.cpp)                
add_dependencies( autoware2vectormap_converter ${catkin_EXPORTED_TARGETS})
target_link_libraries( autoware2vectormap_converter ${catkin_LIBRARIES} autoware_map)

add_executable( autoware_map_loader
                nodes/autoware_map_loader/autoware_map_loader.cpp)
target_link_libraries( autoware_map_loader ${catkin_LIBRARIES} autoware_map)


## Install executables and/or libraries
install(TARGETS autoware_map autoware2vectormap_converter autoware_map_loader
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

## Install project namespaced headers
install(DIRECTORY include/autoware_map/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})
  
install(DIRECTORY launch/
      DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
      PATTERN ".svn" EXCLUDE)
